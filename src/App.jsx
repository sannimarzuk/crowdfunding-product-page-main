import "./App.scss";
import Logo from "./images/logo.svg";
import MasterLogo from "./images/logo-mastercraft.svg";
import { RewardData } from "./Data";
import { useEffect, useState } from "react";
import Check from "./images/icon-check.svg";

function App() {
  const [active, setActive] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [scrolled, setScrolled] = useState(false);
  const [showTY, setShowTY] = useState(false);
  const [bookmarked, setBookmarked] = useState(false);
  const [totalAmountBacked, setTotalAmountBacked] = useState(89914);
  const [totalBackers, setTotalBackers] = useState(5007);

  useEffect(() => {
    const handleScroll = () => {
      const scrollDistance =
        document.body.scrollTop || document.documentElement.scrollTop;
      setScrolled(scrollDistance > 20);
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [scrolled]);

  const handleRadioChange = (event) => {
    const currentBtn = event.target;
    const firstRadio = document.getElementById("first-radio");
    const radioBtns = document.querySelectorAll("[type='radio']");

    radioBtns.forEach((btn) => {
      const card = btn.parentElement.parentElement.parentElement;

      if (btn.checked) {
        card.classList.add("selected");
      } else {
        card.classList.remove("selected");
      }
    });
    if (currentBtn == firstRadio) {
      setShowModal(false);
      setShowTY(true);
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    }
  };

  function toggleModal() {
    setShowModal((prev) => !prev);
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  }
  const handleSubmit = (e) => {
    e.preventDefault();

    const inputValue = parseFloat(e.target.elements.amount.value);

    setTotalAmountBacked((prevTotal) => prevTotal + inputValue);

    setTotalBackers((prevTotal) => prevTotal + 1);

    setShowModal(false);
    setShowTY(true);
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <>
      <header className={scrolled ? "header scrolled" : "header"}>
        <div className="header__container">
          <a href="#">
            <img src={Logo} alt="Logo" width="128" height="20" />
          </a>
          <nav aria-labelledby="primary-navigation">
            <p id="primary-navigation" hidden>
              primary
            </p>
            <button
              type="button"
              className={
                active ? "header__menu-button close" : "header__menu-button"
              }
              aria-expanded="false"
              aria-controls="menu-list"
              aria-label="Toggle Menu"
              onClick={() => (showModal ? null : setActive(!active))}
            >
              <span className="sr-only">Menu</span>
            </button>
            <ul
              id="menu-list"
              role="menu"
              className={active ? "header__list show" : "header__list"}
            >
              <li className="header__item" role="menuitem">
                <a href="#" className="header__link">
                  About
                </a>
              </li>
              <li className="header__item" role="menuitem">
                <a href="#" className="header__link">
                  Discover
                </a>
              </li>
              <li className="header__item" role="menuitem">
                <a href="#" className="header__link">
                  Get Started
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </header>
      {showModal && <div aria-hidden="true" className="bg__overlay"></div>}
      {active && <div aria-hidden="true" className="bg__overlay"></div>}
      {showTY && <div aria-hidden="true" className="bg__overlay"></div>}
      <main>
        <h1 className="sr-only">Crowdfunding product page main</h1>
        <section className="hero__banner"></section>
        <div className="main__content">
          <div className="main__content-logo">
            <img
              src={MasterLogo}
              alt="Mastercraft Logo"
              width="56"
              height="56"
            />
          </div>
          <div className="content__container">
            <section className="top__section">
              <div className="tp-section__text">
                <h2>Mastercraft Bamboo Monitor Riser</h2>
                <p>
                  A beautiful & handcrafted monitor stand to reduce neck and eye
                  strain.
                </p>
              </div>
              <div className="btns__container">
                <button
                  type="button"
                  aria-pressed="false"
                  aria-haspopup="dialog"
                  className="backproject__btn"
                  onClick={toggleModal}
                >
                  Back this project
                </button>
                <div className="bookmark__container">
                  <a
                    href="#"
                    aria-label={`${bookmarked ? "Remove" : "Add"} bookmark`}
                    role="button"
                    aria-pressed={bookmarked}
                    onClick={() => setBookmarked(!bookmarked)}
                  >
                    <div
                      className={`bookmark__img ${
                        bookmarked ? "bookmarked" : ""
                      }`}
                    ></div>
                    <div role="button" type="button" className="button">
                      {bookmarked ? "Bookmarked" : "Bookmark"}
                    </div>
                  </a>
                </div>
              </div>
            </section>
            <section className="middle__section">
              <div className="md-section__text">
                <div
                  className="backers__amount"
                  aria-describedby="amount-description"
                >
                  ${totalAmountBacked.toLocaleString()}{" "}
                  <span>of $100,000 backed</span>
                </div>
                <div id="amount-description" className="sr-only">
                  Amount backed: ${totalAmountBacked.toLocaleString()} of
                  $100,000
                </div>
                <div className="backers__number" aria-label="number of backers">
                  {totalBackers.toLocaleString()} <span>total backers</span>
                </div>
                <div className="days__container" aria-label="days left">
                  56 <span>days left</span>
                </div>
              </div>
              <label
                htmlFor="progress"
                aria-labelledby="progress"
                className="sr-only"
              >
                Crowd Funding Progress
              </label>
              <progress
                value={85}
                max={100}
                role="progressbar"
                id="progress"
                aria-label="Progress: 85%"
              >
                85%
              </progress>
            </section>
            <section className="bottom__section">
              <h2>About this project</h2>
              <div className="about__container">
                <p>
                  The Mastercraft Bamboo Monitor Riser is a sturdy and stylish
                  platform that elevates your screen to a more comfortable
                  viewing height. Placing your monitor at eye level has the
                  potential to improve your posture and make you more
                  comfortable while at work, helping you stay focused on the
                  task at hand.
                </p>
                <p>
                  Featuring artisan craftsmanship, the simplicity of design
                  creates extra desk space below your computer to allow
                  notepads, pens, and USB sticks to be stored under the stand.
                </p>
              </div>
              <div className="reward__container">
                {RewardData.map((item, index) => {
                  return (
                    <div key={index} className={item.class}>
                      <div className="top">
                        <h2>{item.name}</h2>
                        <p>{item.pledge}</p>
                      </div>
                      <div className="text">{item.text}</div>
                      <div className="bottom">
                        <h2>
                          {item.number}
                          <span>{item.inSpan}</span>
                        </h2>
                        <button
                          type="button"
                          aria-label="Select Reward"
                          onClick={toggleModal}
                        >
                          {item.buttonTxt}
                        </button>
                      </div>
                    </div>
                  );
                })}
              </div>
            </section>
          </div>
        </div>
      </main>
      {showModal && (
        <div id="modal" className="modal">
          <div className="top">
            <h2>Back this project</h2>
            <button
              aria-controls="modal"
              type="button"
              className="modal__close"
              aria-label="Close modal"
              onClick={toggleModal}
            ></button>
          </div>
          <div className="text">
            Want to support us in bringing Mastercraft Bamboo Monitor Riser out
            in the world?
          </div>
          <div className="modal-cards__container">
            <div className="card">
              <div>
                <label htmlFor="first-radio">
                  <input
                    type="radio"
                    id="first-radio"
                    name="choice"
                    onChange={handleRadioChange}
                  />
                  Pledge with no reward
                </label>
              </div>
              <div className="card__text">
                Choose to support us without a reward if you simply believe in
                our project. As a backer, you will be signed up to receive
                product updates via email.
              </div>
            </div>
            {RewardData.map((item, index) => {
              return (
                <div className={`card reward ${item.class}`} key={index}>
                  <div className="reward__card-top">
                    <label htmlFor={item.id}>
                      <input
                        type="radio"
                        id={item.id}
                        name="choice"
                        onChange={handleRadioChange}
                      />
                      <div>
                        {item.name}
                        <span>{item.pledge}</span>
                      </div>
                    </label>
                  </div>
                  <div className="reward__card-text">{item.text}</div>
                  <h2>
                    {item.number}
                    <span>{item.inSpan}</span>
                  </h2>
                  <form onSubmit={handleSubmit}>
                    <label htmlFor={item.formInput}>{item.formTitle}</label>
                    <div>
                      <span>$</span>
                      <input
                        type="number"
                        defaultValue={item.formInput}
                        id={item.formInput}
                        name="amount"
                        min={index == 0 ? "25" : "75"}
                        required
                      />
                      <button type="submit">Continue</button>
                    </div>
                  </form>
                </div>
              );
            })}
          </div>
        </div>
      )}
      {showTY && (
        <div className="modal thank-you" aria-labelledby="thank-you-heading">
          <img src={Check} alt="Checkmark icon indicating success" />
          <h2 id="thank-you-heading">Thanks for your support!</h2>
          <p>
            Your pledge brings us one step closer to sharing Mastercraft Bamboo
            Monitor Riser worldwide. You will get an email once our campaign is
            completed.
          </p>
          <button
            aria-label="Close thank you modal"
            onClick={() => setShowTY(!showTY)}
            autoFocus
          >
            Got it!
          </button>
        </div>
      )}

      <footer className="attribution">
        Challenge by
        <a
          href="https://www.frontendmentor.io/"
          target="_blank"
          aria-describedby="Frontend Mentor Website"
          rel="noreferrer"
        >
          Frontend Mentor
        </a>
        Coded by
        <a
          href="https://github.com/Zukizuk"
          target="_blank"
          rel="noopener noreferrer"
          aria-describedby="Marzuk Entsie's Github profile"
        >
          Zuki
        </a>
      </footer>
    </>
  );
}

export default App;
