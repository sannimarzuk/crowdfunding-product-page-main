export const RewardData = [
  {
    name: "Bamboo Stand",
    pledge: "Pledge $25 or more",
    text: `You get an ergonomic stand made of natural bamboo. You've helped us launch our promotional campaign, and 
        you’ll be added to a special Backer member list.`,
    number: "101",
    inSpan: "left",
    buttonTxt: "Select Reward",
    id: "bamboo",
    formTitle: "Enter your pledge",
    formInput: "25",
    class: "",
  },
  {
    name: "Black Edition Stand",
    pledge: "Pledge $75 or more",
    text: `You get a Black Special Edition computer stand and a personal thank you. You’ll be added to our Backer 
        member list. Shipping is included.`,
    number: "64",
    inSpan: "left",
    buttonTxt: "Select Reward",
    id: "black",
    formTitle: "Enter your pledge",
    formInput: "75",
    class: "",
  },
  {
    name: "Mahogany Special Edition",
    pledge: "Pledge $200 or more",
    text: `You get two Special Edition Mahogany stands, a Backer T-Shirt, and a personal thank you. You’ll be added 
        to our Backer member list. Shipping is included.`,
    number: "0",
    inSpan: "left",
    buttonTxt: "Out of Stock",
    id: "mahogany",
    class: "OFS",
    formInput: "200",
  },
];
